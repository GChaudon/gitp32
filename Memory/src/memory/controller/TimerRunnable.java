package memory.controller;

import java.util.TimerTask;

import memory.view.VueMemory;

public class TimerRunnable extends TimerTask {
	private long startTime;
	private VueMemory vue;
	
	public TimerRunnable(long startTime, VueMemory vue) {
		this.startTime = startTime;
		this.vue = vue;
	}
	
	public void run() {
		long diff = System.currentTimeMillis() - startTime;
		vue.updateTimer((int) (diff / 1000));
	}
}
