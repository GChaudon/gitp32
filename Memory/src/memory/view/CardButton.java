package memory.view;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class CardButton extends JButton {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2933439968307585492L;
	private int x;
	private int y;
	
	public CardButton(int x, int y) {
		super("");
		this.x = x;
		this.y = y;
		this.setBackground(Color.white);
		this.setBorder(BorderFactory.createCompoundBorder(
		       BorderFactory.createLineBorder(Color.LIGHT_GRAY, 2), 
		        BorderFactory.createEmptyBorder(20, 20, 20, 20)));
		this.setFocusPainted(false);
	}

	public void setIconFilename(String icon) {
		this.setIcon(new ImageIcon("./src/assets/img/" + icon));
	}
	
	public int getColumn() {
		return this.x;
	}
	
	public int getRow() {
		return this.y;
	}
}
