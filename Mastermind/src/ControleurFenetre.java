import java.awt.event.ActionListener;

public abstract class ControleurFenetre implements ActionListener {
	protected FenetreMastermind fenetre;
	
	public ControleurFenetre(FenetreMastermind fenetre) {
		this.fenetre = fenetre;
	}
}
