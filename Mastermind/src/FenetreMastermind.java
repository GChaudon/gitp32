import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class FenetreMastermind extends JFrame {
	private static final long serialVersionUID = -6585077619031466025L;
	
	private JMenuItem itemNbCouleurs;
	private JMenuItem itemTailleCombinaison;
	private VueMastermind vueMastermind;

	private ControleurTaille controleurTaille;
	private ControleurNbCouleurs controleurNbCouleurs;

	public FenetreMastermind() {
		this.setTitle("Mastermind");
		this.setSize(300, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		JMenuBar menubar = new JMenuBar();

		JMenu jeu = new JMenu("Jeu");

		JMenuItem replay = new JMenuItem("Rejouer");
		replay.addActionListener(new ControleurRejouer(this));

		JMenuItem sauvegarder = new JMenuItem("Sauvegarder");
		sauvegarder.addActionListener(new ControleurSauvegarder(this));
		JMenuItem restaurer = new JMenuItem("Restaurer");
		restaurer.addActionListener(new ControleurRestaurer(this));
		JMenuItem quitter = new JMenuItem("Quitter");
		quitter.addActionListener(new ControleurQuitter(this));

		jeu.add(replay);
		jeu.add(sauvegarder);
		jeu.add(restaurer);
		jeu.add(quitter);

		JMenu options = new JMenu("Options");

		JMenu nbCouleursMenu = new JMenu("Nombre de couleurs");

		this.controleurNbCouleurs = new ControleurNbCouleurs(this);

		for(int i = 2; i <= 6; i++) {
			JMenuItem nbCouleursItem = new JMenuItem(i+"");
			if(i == 6) {
				this.itemNbCouleurs = nbCouleursItem;
				nbCouleursItem.setBackground(Color.red);
				nbCouleursItem.setSelected(true);
			}
			nbCouleursItem.addActionListener(this.controleurNbCouleurs);
			nbCouleursMenu.add(nbCouleursItem);
		}

		JMenu tailleMenu = new JMenu("Taille combinaison");

		this.controleurTaille = new ControleurTaille(this);

		for(int i = 2; i <= 10; i++) {
			JMenuItem tailleItem = new JMenuItem(i+"");
			if(i == 4) {
				this.itemTailleCombinaison = tailleItem;
				tailleItem.setBackground(Color.green);
				tailleItem.setSelected(true);
			}
			tailleItem.addActionListener(this.controleurTaille);
			tailleMenu.add(tailleItem);
		}

		options.add(nbCouleursMenu);
		options.add(tailleMenu);

		menubar.add(jeu);
		menubar.add(options);

		this.setJMenuBar(menubar);

		this.creerNouvelleVueMastermind();

		this.setVisible(true);
	}

	public void changerItemNbCouleurs(JMenuItem item) {
		this.itemNbCouleurs = item;
	}

	public void changerItemTailleCombinaison(JMenuItem item) {
		this.itemTailleCombinaison = item;
	}

	public void creerNouvelleVueMastermind() throws NullPointerException {
		if(this.itemNbCouleurs == null) throw new NullPointerException("Item nb couleurs pas défini");
		if(this.itemTailleCombinaison == null) throw new NullPointerException("Item taille pas défini");
		int nbCouleurs = Integer.parseInt(this.itemNbCouleurs.getText());
		int taille = Integer.parseInt(this.itemTailleCombinaison.getText());
		if(this.vueMastermind != null) {
			this.remove(this.vueMastermind);
		}
		this.vueMastermind = new VueMastermind(nbCouleurs, taille);
		this.add(this.vueMastermind);
		this.revalidate();
		this.repaint();
	}

	public void restaurerVueMastermindFichier(String nomFichier) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(nomFichier));
			if(this.vueMastermind != null) {
				this.remove(this.vueMastermind);
			}
			this.vueMastermind = (VueMastermind) ois.readObject();
			ois.close();
			this.add(this.vueMastermind);
			this.revalidate();
			this.repaint();
		} catch(IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void sauvegarderVueMastermindFichier(String nomFichier) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(nomFichier));
			oos.writeObject(this.vueMastermind);
			oos.flush();
			oos.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
