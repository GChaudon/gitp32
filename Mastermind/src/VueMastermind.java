import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.io.Serializable;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class VueMastermind extends JPanel implements Serializable {
	private static final long serialVersionUID = 6305011884634832495L;
	private int nbCouleurs;
	public static int NBMAX_COMBINAISONS = 10;
	private int taille;

	private Color[] colors = new Color[] {Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW, Color.CYAN, Color.MAGENTA};

	private JTextField[] bPIHM = new JTextField[NBMAX_COMBINAISONS];
	private JTextField[] mPIHM = new JTextField[NBMAX_COMBINAISONS];

	private JTextField[] combinaisonOrdiIHM = new JTextField[this.taille];
	private JButton[][] combinaisonJoueurIHM = new JButton[NBMAX_COMBINAISONS][this.taille];

	public VueMastermind() {
		this(6, 4);
	}
	
	public VueMastermind(int nbCouleurs, int taille) {
		this.nbCouleurs = Math.min(nbCouleurs, this.colors.length);
		this.taille = taille;
		
		this.combinaisonOrdiIHM = new JTextField[this.taille];
		this.combinaisonJoueurIHM = new JButton[NBMAX_COMBINAISONS][this.taille];
		
		ControleurMastermind controleur = new ControleurMastermind(this);
		
		this.setLayout(new BorderLayout());

		JPanel top = new JPanel();

		top.add(new JLabel("Couleurs : "));

		JPanel topRight = new JPanel();
		topRight.setLayout(new GridLayout(1, this.nbCouleurs));

		for(int i = 0; i < this.nbCouleurs; i++) {
			JColorButton colorButton = new JColorButton(this.colors[i]);
			colorButton.addMouseListener(controleur);
			topRight.add(colorButton);
		}

		top.add(topRight);

		this.add(top, BorderLayout.NORTH);

		JPanel center = new JPanel();
		center.setLayout(new GridLayout(10, 2));

		for(int i = 0; i < this.combinaisonJoueurIHM.length; i++) {

			JPanel centerLeft = new JPanel();
			centerLeft.setLayout(new GridLayout(1, this.taille));

			for(int j = 0; j < this.combinaisonJoueurIHM[i].length; j++) {
				JButton tmpButton = new JButton();
				tmpButton.setEnabled(i == 0);
				tmpButton.addMouseListener(controleur);
				this.combinaisonJoueurIHM[i][j] = tmpButton;
				centerLeft.add(tmpButton);
			}

			JPanel centerRight = new JPanel();
			centerRight.setLayout(new GridLayout(2, 2));

			centerRight.add(new JLabel("BP"));
			centerRight.add(new JLabel("MP"));

			JTextField bpField = new JTextField();
			bpField.setEditable(false);
			this.bPIHM[i] = bpField;
			centerRight.add(bpField);

			JTextField mpField = new JTextField();
			mpField.setEditable(false);
			this.mPIHM[i] = mpField;
			centerRight.add(mpField);

			center.add(centerLeft);
			center.add(centerRight);
		}

		this.add(center, BorderLayout.CENTER);


		JPanel bottom = new JPanel();
		bottom.setLayout(new GridLayout(1, 2));

		JPanel bottomLeft = new JPanel();
		bottomLeft.setLayout(new GridLayout(1, 4));

		for(int i = 0; i < this.combinaisonOrdiIHM.length; i++) {
			JTextField tmpField = new JTextField();
			tmpField.setEditable(false);
			this.combinaisonOrdiIHM[i] = tmpField;
			bottomLeft.add(tmpField);
		}

		bottom.add(bottomLeft);

		JButton validateButton = new JButton("Valider");
		validateButton.addMouseListener(controleur);
		bottom.add(validateButton);

		this.add(bottom, BorderLayout.SOUTH);
	
		controleur.initialiser();
	}
	
	public int getNbCouleurs() {
		return this.nbCouleurs;
	}
	
	public int getTaille() {
		return this.taille;
	}

	public void afficherBP(int i, int nbBP) throws IllegalArgumentException {
		if(i < 0 || i > NBMAX_COMBINAISONS - 1) throw new IllegalArgumentException("Out of bounds");
		this.bPIHM[i].setText("" + nbBP);
	}

	public void afficherMP(int i, int nbMP) throws IllegalArgumentException {
		if(i < 0 || i > NBMAX_COMBINAISONS - 1) throw new IllegalArgumentException("Out of bounds");
		this.mPIHM[i].setText("" + nbMP);
	}

	public void activerCombinaison(int i) throws IllegalArgumentException {
		if(i < 0 || i > NBMAX_COMBINAISONS - 1) throw new IllegalArgumentException("Out of bounds");
		for(JButton tmpButton : this.combinaisonJoueurIHM[i]) {
			tmpButton.setEnabled(true);
		}
	}

	public void desactiverCombinaison(int i) throws IllegalArgumentException {
		if(i < 0 || i > NBMAX_COMBINAISONS - 1) throw new IllegalArgumentException("Out of bounds");
		for(JButton tmpButton : this.combinaisonJoueurIHM[i]) {
			tmpButton.setEnabled(false);
		}
	}

	public void afficherCombinaisonOrdinateur(int[] tableauCouleurs) throws IllegalArgumentException {
		for(int i = 0; i < this.combinaisonOrdiIHM.length; i++) {
			this.combinaisonOrdiIHM[i].setBackground(chiffreEnCouleur(tableauCouleurs[i]));
		}
	}

	public boolean appartientCombinaison(JButton btn, int i) throws IllegalArgumentException {
		if(i < 0 || i > NBMAX_COMBINAISONS) throw new IllegalArgumentException("Out of bounds");
		for(JButton tmpButton : this.combinaisonJoueurIHM[i]) {
			if(tmpButton == btn) return true;
		}

		return false;
	}

	public Color chiffreEnCouleur(int i) throws IllegalArgumentException {
		if(i < 0 || i > this.getNbCouleurs() - 1) throw new IllegalArgumentException("Out of bounds");
		return this.colors[i];
	}

	public int couleurEnChiffre(Color c) throws IllegalArgumentException {
		for(int i = 0; i < this.getNbCouleurs(); i++) {
			if(this.colors[i].equals(c)) return i;
		}

		throw new IllegalArgumentException("Couleur non reconnue");
	}

	public boolean combinaisonComplete(int i) throws IllegalArgumentException {
		if(i < 0 || i > VueMastermind.NBMAX_COMBINAISONS - 1) throw new IllegalArgumentException("Out of bounds");
		for(JButton tmpButton : this.combinaisonJoueurIHM[i]) {
			if(!appartientPalette(tmpButton)) return false;
		}

		return true;
	}

	public boolean appartientPalette(JButton btn) {
		for(Color c : this.colors) {
			if(btn.getBackground() == c) return true;
		}

		return false;
	}

	public int[] combinaisonEnEntiers(int i) throws IllegalArgumentException {
		if(i < 0 || i > NBMAX_COMBINAISONS - 1) throw new IllegalArgumentException("Out of bounds");
		if(!combinaisonComplete(i)) throw new IllegalArgumentException("Combinaison incomplete");

		int[] intColors = new int[this.combinaisonJoueurIHM[i].length];
		for(int j = 0; j < this.combinaisonJoueurIHM[i].length; j++) {
			intColors[j] = couleurEnChiffre(this.combinaisonJoueurIHM[i][j].getBackground());
		}

		return intColors;
	}

}
