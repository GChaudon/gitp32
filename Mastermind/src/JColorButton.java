import java.awt.Color;

import javax.swing.JButton;

public class JColorButton extends JButton {
	private static final long serialVersionUID = 1518122745214651687L;
	private Color color;
	
	public JColorButton(Color color) {
		super("");
		this.color = color;
		this.setBackground(this.color);
	}
	
	public Color getColor() {
		return this.color;
	}
}
