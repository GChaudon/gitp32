package ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import chessquito.PartieChessquito;
import chessquito.PartieNonInitialiseeException;

public class ControleurChessquito implements ActionListener {
	private VueChessquito vue;
	private PartieChessquito modele;
	private CaseIHM lastPiece;

	enum EtatPiece {
		FIXE, ANIMEE
	}

	public ControleurChessquito(VueChessquito vue) {
		this.vue = vue;
		this.modele = new PartieChessquito();
		this.modele.initialiser();
	}

	/**
	 * rafraichit la vue du plateau graphique a partir du modele
	 */
	public void rafraichir() {
		try {
			for(int i = 0; i < 4; i++) {
				for(int j = 0; j < 4; j++) {
					String nom = this.modele.getNomPiece(i, j);
					if(nom != null) {
						String couleur = this.modele.getCouleurPiece(i, j);
						vue.positionnerPiece(new PieceIHM(nom, couleur, "Fixe"), i, j);
					} else {
						vue.positionnerPiece(null, i, j);
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof CaseIHM) {
			CaseIHM clickedCase = (CaseIHM) e.getSource();
			int pos[] = {clickedCase.getLigne(), clickedCase.getColonne()};
			System.out.println("Ligne: "+pos[0]+", Colonne: "+pos[1]);
			if(this.lastPiece == null && clickedCase.getIcon() != null && clickedCase.getIcon() instanceof PieceIHM) {
				PieceIHM piece = (PieceIHM) clickedCase.getIcon();
				String nom = piece.getNomPiece();
				String couleur = piece.getCouleurPiece();
				clickedCase.setIcon(new PieceIHM(nom, couleur, "Anime"));
				
				this.lastPiece = clickedCase;
			} else {
				if(this.lastPiece != null && this.lastPiece.getIcon() != null && this.lastPiece.getIcon() instanceof PieceIHM) {
					try {
						this.modele.jouer(this.lastPiece.getLigne(), this.lastPiece.getColonne(), pos[0], pos[1]);	
					} catch(Exception ex) {
						ex.printStackTrace();
					}
					this.lastPiece = null;
					this.rafraichir();
				}
			}
		}
	}
}
