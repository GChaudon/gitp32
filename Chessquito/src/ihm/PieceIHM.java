package ihm;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class PieceIHM extends ImageIcon {
	private static final long serialVersionUID = -7946990811253521193L;
	private String couleurPiece;
	private String nomPiece;
	private String typeImage;
	
	public PieceIHM(String nomPiece, String couleurPiece, String typeImage) {
		this.nomPiece = nomPiece;
		this.couleurPiece = couleurPiece;
		this.typeImage = typeImage;
		try {
			this.setImage(ImageIO.read(new File("src/ihm/images/"+nomPiece+couleurPiece+typeImage+".gif")));
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getCouleurPiece() {
		return this.couleurPiece;
	}
	
	public String getNomPiece() {
		return this.nomPiece;
	}
	
	public String getTypeImage() {
		return this.typeImage;
	}
	
}
