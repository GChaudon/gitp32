package ihm;

import java.awt.Color;

import javax.swing.JButton;

public class CaseIHM extends JButton {
	private static final long serialVersionUID = 5118329522400865624L;
	private int colonne;
	private int ligne;
	
	public CaseIHM(int i, int j, Color couleur) {
		this.ligne = i;
		this.colonne = j;
		this.setBackground(couleur);
	}
	
	public int getColonne() {
		return this.colonne;
	}
	
	public int getLigne() {
		return this.ligne;
	}
	
}
