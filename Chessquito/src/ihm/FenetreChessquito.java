package ihm;

import java.awt.Dimension;

import javax.swing.JFrame;

public class FenetreChessquito extends JFrame {
	public FenetreChessquito() {
		this.setSize(new Dimension(400, 500));
		this.setTitle("Chessquito");
		this.add(new VueChessquito());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
}
