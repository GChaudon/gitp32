
public class Couple {
	private int premier;
	private int second;
	
	public Couple(int premier, int second) {
		this.premier = premier;
		this.second = second;
	}

	public int getPremier() {
		return this.premier;
	}

	public int getSecond() {
		return this.second;
	}
}
