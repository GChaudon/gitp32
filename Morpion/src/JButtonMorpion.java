import javax.swing.JButton;

public class JButtonMorpion extends JButton {
	private Couple couple;
	public JButtonMorpion(String label, int x, int y) {
		super(label);
		this.couple = new Couple(x, y);
	}
	
	public Couple getCouple() {
		return this.couple;
	}
}
