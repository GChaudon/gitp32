import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class VueMorpion extends JPanel {
	private ControleurMorpion controleur;
	private JLabel joueur;
	private JButton effacer;
	private JLabel info;
	private JButtonMorpion[][] boutonsGrille = new JButtonMorpion[ModeleMorpion.TAILLE_GRILLE][ModeleMorpion.TAILLE_GRILLE];
	
	public VueMorpion() {
		this.controleur = new ControleurMorpion(this, 1);
		this.setLayout(new BorderLayout());
		
		JPanel top = new JPanel();
		top.setLayout(new GridLayout(1, 2));
		this.effacer = new JButton("Effacer");
		this.effacer.addMouseListener(new ResetButtonListener(this));
		this.joueur = new JLabel("", SwingConstants.CENTER);
		top.add(this.effacer);
		top.add(this.joueur);
		
		this.add(top, BorderLayout.NORTH);
		
		JPanel center = new JPanel();
		center.setLayout(new GridLayout(3, 3));
		
		for(int y = 0; y < this.boutonsGrille.length; y++) {
			for(int x = 0; x < boutonsGrille.length; x++) {
				this.boutonsGrille[y][x] = new JButtonMorpion("", x, y);
				this.boutonsGrille[y][x].addMouseListener(this.controleur);
				center.add(this.boutonsGrille[y][x]);
			}
		}
		
		this.add(center, BorderLayout.CENTER);
		
		JPanel south = new JPanel();
		this.info = new JLabel("", SwingConstants.CENTER);
		south.add(this.info);
		
		this.add(south, BorderLayout.SOUTH);
	}
	
	public void initialiser() {
		this.controleur.reset();
		for(int y = 0; y < this.boutonsGrille.length; y++) {
			for(int x = 0; x < this.boutonsGrille[y].length; x++) {
				this.boutonsGrille[y][x].setText("");
				this.boutonsGrille[y][x].setEnabled(true);
			}
		}
		
		this.info.setText("");
		this.joueur.setText("Joueur 1");
	}
	
	public void afficherResultat(int numeroJoueur) {
		this.info.setText("Le joueur " + numeroJoueur + " gagne");
	}
	
	public void drawGame() {
		this.info.setText("Match nul");
	}
	
	public void disableButtons() {
		for(int y = 0; y < this.boutonsGrille.length; y++) {
			for(int x = 0; x < this.boutonsGrille[y].length; x++) {
				this.boutonsGrille[y][x].setEnabled(false);
			}
		}
	}
	
	public void afficherJoueurCourant(int numeroJoueur) {
		this.joueur.setText("Joueur " + numeroJoueur);
	}
	
	public Couple coordonneesBtCaseGrille(JButtonMorpion btn) {
		return btn.getCouple();
	}
}
