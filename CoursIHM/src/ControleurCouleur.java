import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class ControleurCouleur extends FocusAdapter {
	private VueCouleur vue;
	
	public ControleurCouleur(VueCouleur vue) {
		this.vue = vue;
	}
	
	@Override
	public void focusLost(FocusEvent arg0) {
		this.vue.setCouleur();
	}
	
}
