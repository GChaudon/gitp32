import java.awt.Canvas;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class VueCouleur extends Panel {
	private TextField t1;
	private TextField t2;
	private TextField t3;
	private Canvas ca;
	
	public static void main(String[] args) {
		Frame f = new Frame();
		f.setTitle("Convertisseur couleur");
		f.setSize(250, 120);
		f.setLayout(new GridLayout(1, 1));
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				System.exit(0);
			}			
		});
		f.add(new VueCouleur());
		f.setVisible(true);
	}
	
	public VueCouleur() {
		ControleurCouleur controller = new ControleurCouleur(this);
		this.setLayout(new GridLayout(1, 2));
		this.ca = new Canvas();
		ca.setBackground(Color.BLACK);
		Panel p = new Panel();
		this.add(this.ca);
		this.add(p);
		p.setLayout(new GridLayout(3,2));
		this.t1 = new TextField("0");
		this.t1.addFocusListener(controller);
		this.t2 = new TextField("0");
		this.t2.addFocusListener(controller);
		this.t3 = new TextField("0");
		this.t3.addFocusListener(controller);
		p.add(new Label("Rouge"));
		p.add(this.t1);
		p.add(new Label("Vert"));
		p.add(this.t2);
		p.add(new Label("Bleu"));
		p.add(this.t3);
		
		this.setVisible(true);
	}
	
	public void setCouleur() {
		int r = Integer.parseInt(this.t1.getText());
		int g = Integer.parseInt(this.t2.getText());
		int b = Integer.parseInt(this.t3.getText());
		this.ca.setBackground(new Color(r, g, b));
	}
}
